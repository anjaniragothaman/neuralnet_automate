load('bundle_vector_length.mat')
load('p_index.mat')
load('sorted_p.mat')
load('bundle_names.mat')

bundle_num = [];
start_bundle = bundle_vector_length + 1;
finish_bundle = [];
for i = 1:length(bundle_vector_length)

    finish_bundle = [finish_bundle sum(bundle_vector_length(1:i))];

end

finish_bundle = [0 finish_bundle];

% for j = 1:length(I)
% 
%     for k = 1:length(bundle_vector_length)
% 
%         if k==1
%            if (I(j) <= finish_bundle(k))
%               bundle_num = [bundle_num; k];
%            end
%         elseif k >=2
%             if (I(j) <= finish_bundle(k) && I(j) > finish_bundle(k-1))
% 
%                  bundle_num = [bundle_num; k];
% 
%             end
%         end
%     end
% end
% 
% 
% %top 10% of sorted p-values bundle locations
% hist(bundle_num(1:8298),19);
% [N,X] = hist(bundle_num(1:8298),19);
% 
% %bundles who have more than 700 p-values in them
% find(N>700);
% bundle_names(find(N>700))


%I could do this instead
%N = histc(I(1:8298),finish_bundle)
[N,edges] = histcounts(I(1:8298),finish_bundle)
bar(edges(1:end-1),N,'histc')
find(histc(I(1:8298),finish_bundle)>700)
bundle_names(find(histcounts(I(1:8298),finish_bundle)>700))