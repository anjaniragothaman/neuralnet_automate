function fa_r = regress(fa,sex,age)

fa_r = [];

x = [sex age];

for i = 1:size(fa,2)
y = fa(:,i);
stats = regstats(y,x,'linear','r');
fa_r = [fa_r stats.r];
end

end