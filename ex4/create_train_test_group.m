clear all
clc
load('subj_info.mat');
load('fa_pts.mat');
indices = crossvalind('kfold',group,10);

    test_ind = (indices == 1);
    cv_ind = (indices == 2);
    train_ind = ~(test_ind | cv_ind);
    sex_test = sex(test_ind,:);
    sex_train = sex(train_ind,:);
    sex_cv = sex(cv_ind,:);
    sum(sex_test==1)
    sum(sex_cv==1)
    sum(sex_train==1)
    sum(sex_test==2)
    sum(sex_cv==2)
    sum(sex_train==2)
