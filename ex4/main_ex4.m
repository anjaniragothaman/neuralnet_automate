%% Initialization
clear; close all; clc


%% =========== Part 1: Loading Data =============
%  We start the exercise by first loading and visualizing the dataset. 

% Load Data
fprintf('Loading FA average and subjects Data ...\n')
[info,info_str]= xlsread('PD_HC_subject_info_fa.xlsx');
%[info,info_str]= xlsread('PD_HC_subject_info_fa_pts_feature_selected.xlsx');


%[info,info_str]= xlsread('PD_HC_subj_info_FAavg_overall.xlsx');

% Get only controls and PD with tremor/postural stability (162 subj)
%main_info = [info(info(:,5)==3,:); info(info(:,9)==4,:); ... 
    %info(info(:,9)==3,:); ... 
    %info(info(:,9)==2,:); ... 
    %info(info(:,9)==1,:)];
%[~,unique_idx] = unique(main_info(:,1),'first');
%info = main_info(unique_idx,:);

subj_id = info(:,1); %subject_id
sex = info(:,3); %F=1,M=2
dx = info(:,5); %1=PD,3=Control (change ctrl from 3 to 2), so PD=1, Ctrl=2
dx(dx(:)==3)=2;
age = info(:,6);
group = info(:,7); %0=swedd,1=M+PD,2=F+PD,3=M+ctrl,4=F+ctrl
                   % for having same ratio of M/F & PD/Ctrl in test/train
                   % ... and Validation
ps = info(:,8); %postural stability of subjects
tremor = info(:,9); %resting state tremor score for subjects
fa_pts = info(:,10:end);
% fa_feature = find(var(fa_pts) > 0.0013);
% size(fa_feature)
% fa_r = regress(fa_pts(:,fa_feature),sex,age);
fa_r = regress(fa_pts,sex,age);

% create an empty array for storing the test accuracies
train_accuracies = [];
train_sensitivity = [];
train_specificity = [];
test_accuracies = [];
test_sensitivity = [];
test_specificity = [];
cv_accuracies = [];
J_error_vec_train = [];
J_error_vec_test = [];

%% stratified cross validation 
indices = crossvalind('kfold',group,10);
%load('CV_indices_2.mat');
iter = 10;
for i = 1:iter
    i1 = i+1;
    if i==10
        i1 = 1;
    end
    
%     train_accuracies = [];
%     test_accuracies = [];
%     J_error_vec_train = [];
%     J_error_vec_test = [];
    
    % create training, cross validation and test set
    test_ind = (indices == i); %cv_ind = (indices == i1);
    %train_ind = ~(test_ind | cv_ind);
    train_ind = ~(test_ind);

y_dx = dx(train_ind,:);
y = y_dx;
y_dx_test = dx(test_ind,:);
%y_dx_cv = dx(cv_ind,:);

X = fa_r(train_ind,:);
X_test = fa_r(test_ind,:);
%X_cv = fa_r(cv_ind,:);
m = size(X, 1);

%% Setup the parameters you will use for this exercise
input_layer_size  = size(X,2);  % 19*15(285) tracts/bundle averages
hidden_layer_size = 25;   % 25 hidden units
num_labels = size(unique(y_dx),1);   % 2 labels, from 1(Female), 2(Male) 
                                  % Dx (3 labels): 3 - ctrl, 1 - PD, 2 - SWEDD

                                  
%% ================ Part 6: Initializing Pameters ================
%  In this part of the exercise, you will be starting to implment a two
%  layer neural network that classifies digits. You will start by
%  implementing a function to initialize the weights of the neural network
%  (randInitializeWeights.m)

%fprintf('\nInitializing Neural Network Parameters ...\n')

initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);


% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];


%% =================== Part 8: Training NN ===================
%  You have now implemented all the code necessary to train a neural 
%  network. To train your neural network, we will now use "fmincg", which
%  is a function which works similarly to "fminunc". Recall that these
%  advanced optimizers are able to train our cost functions efficiently as
%  long as we provide them with the gradient computations.
%
jrange = m;
for j = jrange
    %[i,j]
%fprintf('\nTraining Neural Network... (Uncomment line 149 in fmincg to see cost for each iteration) \n')

%  After you have completed the assignment, change the MaxIter to a larger
%  value to see how more training helps.
options = optimset('MaxIter', 200);

%  You should also try different values of lambda
%lambda_all = linspace(0,0.3,iter);
%lambda_all = [0 0.001 0.003 0.01 0.03 0.1 0.3 1 3 10];
%lambda = lambda_all(i);
lambda = 1;

% Create "short hand" for the cost function to be minimized
% costFunction = @(p) nnCostFunction(p, ...
%                                    input_layer_size, ...
%                                    hidden_layer_size, ...
%                                    num_labels, X, y_dx, lambda);

costFunction = @(p) nnCostFunction(p, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, X(1:j,:), y_dx(1:j), lambda);

% Now, costFunction is a function that takes in only one argument (the
% neural network parameters)
[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

%fprintf('Program paused. Press enter to continue.\n');
%pause;


%% ================= Part 9: Visualize Weights =================
%  You can now "visualize" what the neural network is learning by 
%  displaying the hidden units to see what features they are capturing in 
%  the data.
%{
fprintf('\nVisualizing Neural Network... \n')

displayData(Theta1(:, 2:end));

fprintf('\nProgram paused. Press enter to continue.\n');
pause;
%}

%% ============= Try Learning Curves
% Weight regularization parameter (we set this to 0 here).
% l = 0;
% 
% 
% [J grad J_error]= nnCostFunction(nn_params, input_layer_size, hidden_layer_size, ...
%                    num_labels, X(1:j,:), y(1:j), l);
% J_error_vec_train = [J_error_vec_train J_error];
% 
% [J grad J_error]= nnCostFunction(nn_params, input_layer_size, hidden_layer_size, ...
%                    num_labels, X_cv, y_dx_cv, l);
% J_error_vec_test = [J_error_vec_test J_error];


%% ================= Part 10: Implement Predict =================
%  After training the neural network, we would like to use it to predict
%  the labels. You will now implement the "predict" function to use the
%  neural network to predict the labels of the training set. This lets
%  you compute the training set accuracy.

pred_train = predict(Theta1, Theta2, X);
train_set_accuracy = mean(double(pred_train == y_dx)) * 100;
train_accuracies = [train_accuracies train_set_accuracy];
fprintf('\nTraining Set Accuracy: %f\n', train_set_accuracy);

%compute sensitivity and specificity for training set
CP_train = classperf(y_dx,pred_train);
train_sensitivity = [train_sensitivity CP_train.Sensitivity];
train_specificity = [train_specificity CP_train.Specificity];

pred_test = predict(Theta1, Theta2, X_test);
test_set_accuracy = mean(double(pred_test == y_dx_test)) * 100;
test_accuracies = [test_accuracies test_set_accuracy];
fprintf('\nTest Set Accuracy: %f\n', test_set_accuracy);

%compute sensitivity and specificity for test set
CP_test = classperf(y_dx_test,pred_test);
test_sensitivity = [test_sensitivity CP_test.Sensitivity];
test_specificity = [test_specificity CP_test.Specificity];

end %for j=1:m loop end

%Plot training_error and test_error
%subplot(10,1,i);
% plot(jrange, J_error_vec_train, jrange, J_error_vec_test);
% title(sprintf('Learning Curve (lambda = %f)', lambda));
% xlabel('Number of training examples');
% ylabel('Error');
% legend('Train', 'Cross Validation');
% 
% %mean test set accuracy for 10 iterations
% figure;
% %subplot(10,2,i);
% plot(jrange, train_accuracies, jrange, test_accuracies);
% title(sprintf('Accuracy (lambda = %f)', lambda));
% xlabel('Number of training examples');
% ylabel('Accuracy');
% legend('Train', 'Cross Validation');
%lambda_all

% pred = predict(Theta1, Theta2, X_cv);
% cv_set_accuracy = mean(double(pred == y_dx_cv)) * 100;
% cv_accuracies = [cv_accuracies cv_set_accuracy];
% fprintf('\nValidation Set Accuracy: %f\n', cv_set_accuracy);

end %for i=1:iter loop end

fprintf('\nMean train set accuracy: %f\n',mean(train_accuracies));
fprintf('\nMean train set Sensitivity: %f\n',mean(train_sensitivity));
fprintf('\nMean train set Specificity: %f\n',mean(train_specificity));

fprintf('\nMean test set accuracy: %f\n',mean(test_accuracies));
fprintf('\nMean test set Sensitivity: %f\n',mean(test_sensitivity));
fprintf('\nMean test set Specificity: %f\n',mean(test_specificity));
%fprintf('\nMean validation Set Accuracy: %f\n', mean(cv_accuracies));

% pred = predict(Theta1, Theta2, X_cv);
% cv_set_accuracy = mean(double(pred == y_dx_cv)) * 100;
% fprintf('\nValidation Set Accuracy: %f\n', cv_set_accuracy);

